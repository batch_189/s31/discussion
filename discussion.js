let http = require ('http');
let port = 4000

http.createServer(function(request, response) {
    response.writeHead(200, {'content-type': 'text/plain'});

    response.end('Hello');
}).listen(port);

console.log(`Server is running at localhost: ${port}`);
