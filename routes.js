const http = require ('http');
const port = 4000

const server = http.createServer(function (req, res) {
    // You can use req.url to get the current destination of the user  in the browser. you can then check if the current destination of the user matches with any endpoint and if it does, do the respective processes for each endpoint.
    if (req.url == '/greeting'){
        res.writeHead(200, {'content-Type': 'text/plain'});
        res.end('Heeelooo');
    } else if (req.url == '/homepage') {
        res.writeHead(200, {'content-Type': 'text/plain'});
        res.end('This is the Homepage');
    } else { // If none of the endpoint match, else condition will show up.
        res.writeHead(404, {'content-Type': 'text/plain'});
        res.end('Page is not available.');
    }
});

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);